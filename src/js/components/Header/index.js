import React from 'react';
import PropTypes from 'prop-types';

import css from 'style/components/Toolbar';

const Header = ({ title, children }) => (
  <header className={ css.toolbar }>
    <div className={ css.toolbar__wrap }>
      <h3 className={ css.toolbar__title }>{ title }</h3>
      { children }
    </div>
  </header>
);

Header.propTypes = {
  title: PropTypes.string,
  children: PropTypes.any,
};

export default Header;

