import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

import WithPageOptions from 'components/WithPageOptions';

import css from 'style/components/Button';

@WithPageOptions()
export default class AddResource extends Component {
  static propTypes = {
    pageOptions: PropTypes.object,
  };

  render() {
    const { pageOptions: { route, text } } = this.props;

    return (
      <div className={ css.button__add }>
        <Link to={ route } title={ text }>
          <FloatingActionButton>
            <ContentAdd />
          </FloatingActionButton>
        </Link>
      </div>
    );
  }
}

