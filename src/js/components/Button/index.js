import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { RaisedButton, FlatButton, IconButton } from 'material-ui';

import Fetching from 'components/Fetching';

import css from 'style/components/Button';

const Button = ({
  typeButton = 'raise',
  to,
  iconName,
  className,
  onClick,
  primaryClass,
  secondaryClass,
  disabledClass,
  isFetching,
  children,
  ...button
}) => (
  <span>
    {typeButton === 'raise' &&
      <RaisedButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button }>
        <Fetching isFetching={ isFetching } isFetchingButton='button' size={ 24 } color='#fff' thickness={ 5 } />
      </RaisedButton>
    }

    {typeButton === 'link' &&
      <NavLink to={ to } className={ className }>
        <RaisedButton label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
      </NavLink>
    }

    {typeButton === 'flat' &&
      <FlatButton className={ className } label={ children } primary={ primaryClass } secondary={ secondaryClass } disabled={ disabledClass } onClick={ onClick } { ...button } />
    }

    {typeButton === 'icon' &&
      <IconButton className={ className } onClick={ onClick }>
        {iconName ? <i className={ css.materialIcons }>{ iconName }</i> : { ...children } }
      </IconButton>
    }
  </span>
);

Button.propTypes = {
  typeButton: PropTypes.string,
  to: PropTypes.string,
  iconName: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func,
  primaryClass: PropTypes.bool,
  secondaryClass: PropTypes.bool,
  disabledClass: PropTypes.bool,
  isFetching: PropTypes.bool,
  children: PropTypes.any,
};

export default Button;
