import { isEmpty, isNumber } from 'lodash';

export const required = (value) => {
  return isEmpty(value) && !isNumber(value) ? 'is required' : undefined;
};

export const email = (value) => {
  return value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value.trim()) ? 'is invalid' : undefined;
};

export const minLength = (min) => (value) => {
  return value && value.length < min ? `must be at least ${ min } characters` : undefined;
};

export const maxLength = (max) => (value) => {
  return value && value.length > max ? `must be ${ max } characters or less` : undefined;
};

export const exactLength = (exact) => (value) => {
  return value && value.length !== exact ? `must be ${ exact } characters` : undefined;
};

export const minLength5 = minLength(5);
export const minLength3 = minLength(3);
export const minLength2 = minLength(2);
export const exactLength7 = exactLength(7);
