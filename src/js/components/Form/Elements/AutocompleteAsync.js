import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { debounce } from 'lodash';

import * as getSupplies from 'redux/modules/supplies';
import Autocomplete from 'components/Autocomplete';

@connect((state, props) => ({
  supply: state.supplies[props.options.supply],
}),
(dispatch) => ({
  getSupplies: bindActionCreators(getSupplies, dispatch),
})
)
export default class AutocompleteAsync extends Component {
  static propTypes = {
    label: PropTypes.string,
    input: PropTypes.object,
    supply: PropTypes.object,
    getSupplies: PropTypes.object,
    meta: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.handleLoadSuggestions = debounce(this.handleLoadSuggestions, 300);
  }

  state = {
    data: [],
    loaded: false,
    isActive: false,
  };

  handleFocus = () => {
    this.setState({
      data: [],
      isActive: true,
    });
  };

  handleBlur = () => {
    this.setState({
      data: [],
      isActive: false,
    });
  };

  handleSelect = (item) => {
    const { input: { onChange } } = this.props;
    onChange(item.id);
    this.setState({
      data: [],
    });
  };

  handleLoadSuggestions = async (value) => {
    if (value) {
      const { options, getSupplies } = this.props;
      const suggestions = await getSupplies[options.supply](value);
      this.saveSuggestions(suggestions);
    }
  };

  saveSuggestions = (suggestions) => {
    this.setState({
      data: suggestions.result,
    });
  };

  render() {
    const { data, loaded } = this.state;
    const { label, input, supply: { loading }, meta } = this.props;

    return (
      <div>
        <Autocomplete
          meta={ meta }
          input={ input }
          label={ label }
          loading={ loading }
          loaded={ loaded }
          suggestions={ data }
          onFocus={ this.handleFocus }
          onBlur={ this.handleBlur }
          onSelect={ this.handleSelect }
          loadSuggestions={ this.handleLoadSuggestions }
        />
      </div>
    );
  }
}
