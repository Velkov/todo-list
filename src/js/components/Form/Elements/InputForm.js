import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Input } from './index';

export default class InputForm extends Component {
  static propTypes = {
    input: PropTypes.object,
  };

  state = {
    value: '',
  };

  componentWillMount() {
    const { input: { value } } = this.props;

    if (value) {
      this.setState({ value });
    }
  }

  componentWillReceiveProps({ input: { value } }) {
    if (value !== this.props.input.value) {
      this.setState({ value });
    }
  }

  onChange = (e, value) => {
    this.setState({ value });
  };

  render() {
    const { value = '' } = this.state;

    return (
      <Input { ...this.props } value={ value } onChange={ this.onChange } />
    );
  }
}
