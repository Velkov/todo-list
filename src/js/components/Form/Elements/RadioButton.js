import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import Button from 'components/Button';

import css from 'style/components/RadioButton';

const style = {
  color: 'red',
  fontSize: '12px',
};

const RadioButton = ({ label, input: { name, value }, options: { data }, fullWidth, onChange, disabled, meta: { error, touched } }) => (
  <div className={ css.formGroup }>
    <h4>{ label }</h4>
    <div className={ css.formGroup__wrap }>
      <div className={ css.radioButton__group }>
        {data.map((item) => {
          return (
            <div key={ item.id } className={ cx(css.radioButton, { [css.radioButton_full]: fullWidth }) }>
              <input className={ css.radioButton__input } id={ `${ name }_${ item.id }` } type='radio' name={ name } onChange={ onChange } value={ item.id } checked={ item.id === value } disabled={ disabled } />
              <Button className={ css.radioButton__label } label={ item.value } htmlFor={ `${ name }_${ item.id }` } typeButton='flat' containerElement='label' />
            </div>
          );
        })}
      </div>
      { touched && error && <span style={ style }>{ label } { error }</span> }
    </div>
  </div>
);

RadioButton.propTypes = {
  disabled: PropTypes.bool,
  fullWidth: PropTypes.bool,
  label: PropTypes.string,
  input: PropTypes.object,
  options: PropTypes.object,
  meta: PropTypes.object,
  onChange: PropTypes.func,
};

export default RadioButton;
