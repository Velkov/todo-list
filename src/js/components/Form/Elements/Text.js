import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const Input = ({ input, label, type }) => (
  <TextField floatingLabelText={ label } type={ type } fullWidth={ true } value={ input.value } />
);

Input.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
};

export default Input;
