import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CheckboxButton from './CheckboxButton';

@connect((state, props) => ({
  supply: state.supplies[props.options.supply],
}))
export default class CheckboxSupply extends Component {
  static propTypes = {
    supply: PropTypes.object,
    input: PropTypes.object,
  };

  state = {
    values: [],
  };

  componentWillReceiveProps(nextProps) {
    const { input: { value } } = nextProps;
    this.setState({
      values: value,
    });
  }

  handleChange = (event) => {
    let value = [...this.props.input.value];
    if (value.includes(+event.target.value)) {
      const index = value.indexOf(+event.target.value);
      value.splice(index, 1);
    } else {
      value = [...this.props.input.value, +event.target.value];
    }
    this.setState({
      values: value,
    });
    this.props.input.onChange(value);
  };

  render() {
    const { values } = this.state;
    const { supply: { data } } = this.props;
    return (
      <CheckboxButton { ...this.props } options={ { data } } onChange={ this.handleChange } values={ values } />
    );
  }
}
