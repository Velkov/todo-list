import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const Input = ({ label, value, onChange, ...input }) => (
  <TextField
    floatingLabelText={ label }
    fullWidth={ true }
    value={ value }
    onChange={ onChange }
    { ...input }
  />
);

Input.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};

export default Input;
