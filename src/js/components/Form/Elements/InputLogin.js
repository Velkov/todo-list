import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';

const InputLogin = ({ input, label, type, meta: { error, touched } }) => (
  <TextField floatingLabelText={ label } type={ type } fullWidth={ true } errorText={ touched && error ? `${ label } ${ error }` : '' } { ...input } />
);

InputLogin.propTypes = {
  input: PropTypes.object,
  name: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object,
};

export default InputLogin;
