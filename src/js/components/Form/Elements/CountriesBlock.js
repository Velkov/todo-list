import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import { connect } from 'react-redux';

import CountriesList from 'components/Countries/CountriesList';
import { countries as getCountries } from 'redux/modules/supplies';

@connect((state) => ({
  countries: state.supplies.countries,
}),
{ getCountries }
)
export default class CountriesBlock extends Component {
  static propTypes = {
    input: PropTypes.object,
    countries: PropTypes.object,
    getCountries: PropTypes.func,
  };

  state = {
    selectedValues: [],
    showCountries: false,
  };

  async componentWillMount() {
    await this.props.getCountries();
  }

  componentWillReceiveProps(nextProps) {
    const { selectedValues } = this.state;
    const { input: { onChange, value } } = nextProps;
    if (selectedValues.length > 0) {
      onChange(selectedValues);
    } else {
      this.setState({
        selectedValues: value,
      });
    }
  }

  getCountryById = (id) => {
    const { countries: { data } } = this.props;
    const country = data.find((item) => {
      return item.id === id;
    });
    return country.value;
  };

  handleAddCountries = () => {
    this.setState({
      showCountries: true,
    });
  };

  handleRemoveAllCountries = async () => {
    const { input: { onChange } } = this.props;
    await this.setState({
      selectedValues: [],
    });
    onChange([]);
  };

  removeCountryById = async (id) => {
    const { selectedValues } = this.state;
    const { input: { onChange } } = this.props;
    const selected = selectedValues.filter((item) => item !== id);
    await this.setState(() => ({
      selectedValues: selected,
    }));
    onChange(selected);
  };

  handleCountriesListCancel = () => {
    this.setState({
      showCountries: false,
    });
  };

  handleCountriesListSubmit = (values) => {
    const { input: { onChange } } = this.props;
    this.setState({
      showCountries: false,
      selectedValues: values,
    });
    onChange(values);
  };

  render() {
    const { selectedValues, showCountries } = this.state;
    const { countries: { data } } = this.props;
    return (
      <div>
        <Button onClick={ () => this.handleRemoveAllCountries() }>&#x2716;</Button>
        <div>
          {data.length > 0 && selectedValues.length > 0 && selectedValues.map((item) => {
            return (
              <li key={ item }>
                <span>{ this.getCountryById(item) }</span>
                <Button onClick={ () => this.removeCountryById(item) }> x</Button>
              </li>
            );
          })}
        </div>
        <Button onClick={ () => this.handleAddCountries() }>+</Button>

        <CountriesList
          selected={ selectedValues }
          showCountries={ showCountries }
          onCancel={ this.handleCountriesListCancel }
          onSubmit={ this.handleCountriesListSubmit }
        />
      </div>
    );
  }
}
