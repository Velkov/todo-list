import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField } from 'material-ui';
import RemoveRedEye from 'material-ui/svg-icons/image/remove-red-eye';
import cx from 'classnames';

import css from 'style/components/FormElements.scss';

class InputPassword extends Component {
  state = {
    showPass: false,
  };

  componentDidMount() {
    this.checkAutofill();
  }

  componentWillUnmount() {
    this.clearAutofillInterval();
  }

  checkAutofill = () => {
    this.autofillInterval = window.setInterval(() => {
      const hasAutoFill = document.querySelector(`#${ this.password.uniqueId }:-webkit-autofill`) !== null;
      if (hasAutoFill) {
        this.clearAutofillInterval();
        this.password.setState({ ...this.password.state, hasValue: true });
      }
    }, 700);
  };

  clearAutofillInterval = () => {
    if (this.autofillInterval) {
      window.clearInterval(this.autofillInterval);
    }
  };

  togglePass = () => {
    this.setState((prevState) => ({
      showPass: !prevState.showPass,
    }));
  };

  render() {
    const { input, label, type, meta: { error, touched } } = this.props;

    return (
      <div className={ css.auth__relative }>
        <TextField ref={ (ref) => this.password = ref } fullWidth={ true } floatingLabelText={ label } type={ (this.state.showPass && type === 'password') ? 'text' : type } errorText={ touched && error ? `${ label } ${ error }` : '' } { ...input } />
        <span className={ cx(css.auth__img, css.auth__img_eye) } onMouseOver={ this.togglePass } onMouseOut={ this.togglePass }>
          <RemoveRedEye color='#575757' hoverColor='#999' />
        </span>
      </div>
    );
  }
}

InputPassword.propTypes = {
  input: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string,
  meta: PropTypes.object,
};

export default InputPassword;
