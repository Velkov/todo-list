import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

import { CircularProgress, LinearProgress } from 'material-ui';

import css from 'style/components/Fetching';

import LoadSvg from './LoadSvg';

const styles = {
  line: {
    position: 'absolute',
    borderRadius: 0,
    top: 0,
    left: 0,
    height: '3px',
    opacity: 0.24,
  },
  lineBottom: {
    position: 'absolute',
    borderRadius: 0,
    bottom: 0,
    left: 0,
    height: '3px',
    opacity: 0.24,
  },
};

const Fetching = ({ position, color = '#c4cdcf', size = 26, thickness = 5, typeProgress = 'circle', showBottomLine, isFetching, isFetchingButton, children }) => (
  <section className={ cx({ [css.fetching__button]: isFetchingButton }) }>
    {typeProgress === 'circleFetch' &&
      <CircularProgress color={ color } size={ size } thickness={ thickness } />
    }
    {isFetching && <div>
      {typeProgress === 'line' && <div>
        <LinearProgress mode='indeterminate' style={ styles.line } />
        {showBottomLine && <LinearProgress mode='indeterminate' style={ styles.lineBottom } />}
      </div>}

      {typeProgress === 'load' && <div className={ cx(css.fetching__load, css[`fetching__load_${ color }`]) }>
        <LoadSvg />
      </div>}
    </div>}

    <article className={ css.fetching__relative }>
      {isFetching && <div className={ cx(css.fetching__wrap, css[`fetching__wrap_${ position }`]) }>
        {typeProgress === 'circle' && <CircularProgress color={ color } size={ size } thickness={ thickness } />}
      </div>}

      { children }
    </article>
  </section>
);

Fetching.propTypes = {
  typeProgress: PropTypes.string,
  color: PropTypes.string,
  position: PropTypes.string,
  isFetchingButton: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number,
  showBottomLine: PropTypes.bool,
  isFetching: PropTypes.bool,
  children: PropTypes.any,
};

export default Fetching;
