import React from 'react';
import PropTypes from 'prop-types';

const Dumb = ({ value }) => (
  <p>
    { value }
  </p>
);

Dumb.propTypes = {
  value: PropTypes.any,
};

export default Dumb;
