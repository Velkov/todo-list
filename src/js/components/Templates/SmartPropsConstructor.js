import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Smart extends Component {
  static propTypes = {
    optionalArray: PropTypes.array,
    optionalBool: PropTypes.bool,
    optionalFunc: PropTypes.func,
    optionalNumber: PropTypes.number,
    optionalObject: PropTypes.object,
    optionalString: PropTypes.string,
    optionalSymbol: PropTypes.symbol,
    optionalElement: PropTypes.element,
    optionalElement: PropTypes.node,
    requiredAny: PropTypes.any.isRequired,
    optionalEnum: PropTypes.oneOf(['News', 'Photos']),
    optionalMessage: PropTypes.instanceOf(Message),
    optionalArrayOf: PropTypes.arrayOf(PropTypes.number),
    optionalUnion: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.instanceOf(Message)
    ]),
    optionalObjectWithShape: PropTypes.shape({
      color: PropTypes.string,
      fontSize: PropTypes.number
    }),
    requiredFunc: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        Smart
      </div>
    );
  }
}
