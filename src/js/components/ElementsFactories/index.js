import React from 'react';
import PropTypes from 'prop-types';

import { formElement } from 'components/Form';
import INPUTS from 'config/elements/inputs';

const ElementFactories = ({ type, value, handleChange, ...props }) => {
  const item = INPUTS[type];
  const Element = formElement[item.component];

  return <Element { ...item } { ...props } input={ { value, onChange: (selected) => handleChange({ [item.name]: selected }) } } meta={ { } } />;
};

ElementFactories.propTypes = {
  type: PropTypes.string,
  value: PropTypes.string,
  handleChange: PropTypes.func,
};

export default ElementFactories;
