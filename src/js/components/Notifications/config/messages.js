const MESSAGES = {
  error: {
    id: 1,
    type: 'error',
    title: 'Error',
    icon: 'error_outline',
  },
  info: {
    id: 2,
    type: 'info',
    title: 'Information',
    icon: 'info_outline',
  },
  success: {
    id: 3,
    type: 'success',
    title: 'Success',
    icon: 'check_circle',
  },
};

export default MESSAGES;
