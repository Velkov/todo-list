import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { add, remove } from 'redux/modules/notifications';
import Message from 'components/Notifications/Message';

import css from 'style/components/Notifications.scss';

@connect((state) => ({
  notifications: state.notifications,
}), {
  add, remove,
})
export default class Notifications extends Component {
  static propTypes = {
    notifications: PropTypes.object,
    add: PropTypes.func,
    remove: PropTypes.func,
  };

  componentWillMount() {
    window.addEventListener('panel-notifications', this.handleEvent);
  }

  componentWillUnmount() {
    window.removeEventListener('panel-notifications', this.handleEvent);
  }

  handleEvent = (e) => {
    this.props.add(e.detail);
  };

  handleRemove = (id) => {
    this.props.remove(id);
  };

  render() {
    const { data } = this.props.notifications;

    return (
      <ul className={ css.notifications }>
        <CSSTransitionGroup className={ css.notifications } component='ul' transitionName={ { enter: css.notifications__item_show, leave: css.notifications__item_hide } } transitionEnterTimeout={ 1200 } transitionLeaveTimeout={ 600 }>
          {data.map(({ id, type, text, details }) => {
            return <Message key={ id } id={ id } type={ type } text={ text } details={ details } handleRemove={ this.handleRemove } />;
          })}
        </CSSTransitionGroup>
      </ul>
    );
  }
}
