import CustomEvent from 'custom-event';
import { get as getValue } from 'lodash';

export function dispatchError(text, details) {
  const event = new CustomEvent('panel-notifications', {
    detail: {
      id: Date.now(),
      type: 'error',
      text,
      details,
    },
  });

  window.dispatchEvent(event);
}

export function dispatchInfo(text, details) {
  const event = new CustomEvent('panel-notifications', {
    detail: {
      id: Date.now(),
      type: 'info',
      text,
      details,
    },
  });

  window.dispatchEvent(event);
}

export function dispatchSuccess(text, details) {
  const event = new CustomEvent('panel-notifications', {
    detail: {
      id: Date.now(),
      type: 'success',
      text,
      details,
    },
  });

  window.dispatchEvent(event);
}

export function dispatchErrorRequest(res) {
  const error = getValue(res, 'error.messages[0]');
  dispatchError(error, JSON.stringify(res, null, ' '));
}
