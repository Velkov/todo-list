import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button';
import cx from 'classnames';

import MESSAGES from 'components/Notifications/config/messages';
import LinearProgress from 'material-ui/LinearProgress';

import css from 'style/components/Notifications.scss';

export default class Message extends Component {
  static propTypes = {
    id: PropTypes.number,
    type: PropTypes.string,
    text: PropTypes.string,
    details: PropTypes.string,
    handleRemove: PropTypes.func,
  };

  state = {
    showDetails: false,
    completed: 0,
  };

  componentWillMount() {
    this.initTimer();
  }

  componentWillUnmount() {
    this.deleteTimer();
  }

  onEnter = () => {
    this.deleteTimer();
  };

  onLeave = () => {
    this.initTimer();
  };

  styles = {
    minWidth: 'initial',
    height: '3.2rem',
    lineHeight: '3.4rem',
    marginTop: '.5rem',
    paddingLeft: '0',
  };

  progress(completed) {
    if (completed > 100) {
      this.setState({ completed: 100 });
      setTimeout(() => this.removeNotifications(), 500);
    } else {
      this.setState({ completed });
      this.timer = setTimeout(() => this.progress(completed + 2.5), 120);
    }
  }

  toggleDetails = () => {
    this.setState((prevState) => ({
      showDetails: !prevState.showDetails,
    }));
  };

  removeNotifications = () => {
    this.props.handleRemove(this.props.id);
  };

  initTimer = () => {
    this.timer = setTimeout(() => this.progress(2), 1000);
  };

  deleteTimer = () => {
    clearTimeout(this.timer);
    this.setState({ completed: 0 });
  };

  render() {
    const { id, type, text, details, handleRemove } = this.props;
    const { showDetails } = this.state;
    const message = MESSAGES[type];

    return (
      <li className={ css.notifications__item } onMouseEnter={ this.onEnter } onMouseLeave={ this.onLeave }>
        <div className={ css.notifications__content }>
          <div className={ css.notifications__wrap }>
            <div className={ cx(css.notifications__icon, css[`notifications__icon_${ message.type }`]) }>
              <i className={ css.materialIcons }>{ message.icon }</i>
            </div>
            <div>
              <h5 className={ cx(css.notifications__title, css[`notifications__title_${ message.type }`]) }>
                { message.title }
              </h5>
              <p className={ css.notifications__text }>{ text }</p>
              <Button className={ css.notifications__close } typeButton='icon' iconName='close' type='submit' onClick={ () => handleRemove(id) } />
              {details &&
                <div className={ css.notifications__btn }>
                  <Button typeButton='flat' label='Details' labelPosition='before' type='submit' onClick={ this.toggleDetails } icon={ <i className={ css.materialIcons }>{ showDetails ? 'arrow_drop_up' : 'arrow_drop_down' }</i> } style={ this.styles } />
                </div>
              }
            </div>
            <LinearProgress className={ cx(css.notifications__progress, css[`notifications__progress_${ message.type }`]) } mode='determinate' value={ this.state.completed } />
          </div>
          {showDetails &&
            <div className={ css.notifications__details }>
              <pre className={ css.notifications__text }>{ details }</pre>
            </div>
          }
        </div>
      </li>
    );
  }
}
