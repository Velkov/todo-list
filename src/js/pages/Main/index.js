import React, { Component } from 'react';
import cx from 'classnames';

import Header from 'components/Header';

import css from 'style/pages/Main';

export default class Main extends Component {
  render() {
    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header title='Home' />
        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ cx(css.grid__item, css.grid__item_12) }>
              <div className={ css.grid__ibox }>
                <h3 className={ css.grid__title }>Hello!</h3>
                <div>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi, accusantium.</p>
                </div>
              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
