import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { connect } from 'react-redux';

import { get as getTodoList } from 'redux/modules/todo';
import Button from 'components/Button';
import Fetching from 'components/Fetching';
import Header from 'components/Header';

import css from 'style/pages/Articles';

@connect((state, props) => ({
  todoList: state.todo.get,
  todoListId: props.match.params.id,
}),
{ getTodoList }
)
export default class Article extends Component {
  static propTypes = {
    todoList: PropTypes.object,
    todoListId: PropTypes.string,
    getTodoList: PropTypes.func,
  };

  async componentWillMount() {
    this.props.getTodoList();
  }

  render() {
    const { todoListId, todoList: { data, loading, loaded } } = this.props;
    const article = data.find(item => item.id === +todoListId);

    return (
      <section className={ cx(css.section, css.section_gray) }>
        <Header title='Home' />
        <section className={ cx(css.wrap, css.wrap_medium) }>
          <ul className={ css.grid }>
            <li className={ css.grid__item }>
              <div className={ css.grid__ibox }>
                <Fetching isFetching={ loading } size={ 45 } thickness={ 8 } position='initial'>
                  {loaded && <div>
                    <h3 className={ css.grid__title }>{ article.todo_title }</h3>
                    <h4 className={ css.todoList__date }>{ article.todo_date }</h4>

                    {article.todo_url && <p>
                      <img src={ `${ article.todo_url }` } alt='' />
                    </p>}

                    <p>{ article.todo_desc }</p>
                    <Button typeButton='link' to='/articles' className={ css.todoList__link }>Back to articles</Button>
                  </div>}
                </Fetching>
              </div>
            </li>
          </ul>
        </section>
      </section>
    );
  }
}
