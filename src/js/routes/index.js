import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import history from 'routes/history';

import App from 'containers/App';
import Member from 'containers/Member';

import Main from 'pages/Main';

import Articles from 'pages/Articles';
import Article from 'pages/Article';

import NotFound from 'pages/NotFound';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <App>
          <Member>
            <Switch>
              <Route exact path='/' render={ (props) => <Main { ...props } /> } />

              <Route exact path='/articles' render={ (props) => <Articles { ...props } /> } />
              <Route path='/articles/:id' render={ (props) => <Article { ...props } /> } />

              <Route component={ NotFound } />
            </Switch>
          </Member>
        </App>
      </Router>
    );
  }
}
