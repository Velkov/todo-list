import React from 'react';
import PropTypes from 'prop-types';

import Navigation from 'containers/Navigation';
import Notifications from 'components/Notifications';

import css from 'style/containers/Member';

const MemberLayout = ({ children, ...props }) => (
  <div className={ css.member }>
    <div className={ css.member__item }>
      <Navigation { ...props } />
      <main className={ css.member__inner }>
        <Notifications />
        { children }
      </main>
    </div>
  </div>
);

MemberLayout.propTypes = {
  children: PropTypes.element,
};

export default MemberLayout;
