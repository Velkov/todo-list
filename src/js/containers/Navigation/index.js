import React, { Component } from 'react';
import { withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import cx from 'classnames';

import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { MenuItem } from 'material-ui';
import Button from 'components/Button';

import css from 'style/containers/Navigation';

@withRouter
export default class Navigation extends Component {
  state = {
    isMenuOpen: false,
    expandedItems: [],
  };

  componentWillMount() {
    const isLeftMenuOpen = window.localStorage.getItem('isLeftMenuOpen') === 'true';

    if (isLeftMenuOpen) {
      this.setState({
        isMenuOpen: isLeftMenuOpen,
      });
      this.addGlobalClass('leftMenu_open');
    }
  }

  toggleMenu = () => {
    const { isMenuOpen } = this.state;
    this.setState({ isMenuOpen: !isMenuOpen });
    window.localStorage.setItem('isLeftMenuOpen', !isMenuOpen);
  };

  mouseMenuOver = () => {
    if (!this.state.isMenuOpen) {
      this.addGlobalClass('leftMenu_open');
    }
  };

  mouseMenuOut = () => {
    if (!this.state.isMenuOpen) {
      this.removeGlobalClass('leftMenu_open');
    }
  };

  addGlobalClass(className) {
    document.body.classList.add(className);
  }

  removeGlobalClass(className) {
    document.body.classList.remove(className);
  }

  handleToggleExpandItem = (item) => {
    this.setState(({ expandedItems }) => {
      let items;
      if (expandedItems.includes(item)) {
        items = expandedItems.filter(expanded => expanded !== item);
      } else {
        items = [...expandedItems, item];
      }
      return { expandedItems: items };
    });
  }

  render() {
    const { isMenuOpen, expandedItems } = this.state;

    return (
      <aside className={ cx({ [css.aside]: true, [css.aside_noanimated]: isMenuOpen, [css.aside__global]: true }) } onMouseEnter={ this.mouseMenuOver } onMouseLeave={ this.mouseMenuOut }>
        <section className={ css.aside__wrap }>
          <nav className={ css.nav }>

            <header className={ cx(css.nav__item, css.header) }>
              <NavLink className={ css.logo } to='/'>
                <span className={ css.logo__tukan } />
                <span className={ cx(css.logo__slogan, css.aside__hideitem) }>
                  <span>Project</span>
                  <span className={ css.userName }>
                    Tra la la
                  </span>
                </span>
              </NavLink>
              <span className={ cx({ [css.aside__pin]: true, [css.aside__pin_fix]: isMenuOpen, [css.aside__hideitem]: true }) } onClick={ this.toggleMenu } >
                <Button typeButton='icon' className={ css.effect_white }>
                  <svg width='1.4rem' height='1.7rem' viewBox='0 0 14 17'>
                    <path d='M3.792,12.992L2.22,16.235h0a0.535,0.535,0,0,0,.229.695,0.568,0.568,0,0,0,.733-0.155l0,0,2.076-2.96Zm6.448-1.719L12.43,5.706a1.1,1.1,0,0,0,1.439-.4,1.035,1.035,0,0,0-.4-1.435L11.04,2.5l-0.031-.018L8.576,1.116A1.1,1.1,0,0,0,7.1,1.5a1.034,1.034,0,0,0,.36,1.414L3.6,7.543c-0.947-.082-2.932.174-3.49,1.115a0.587,0.587,0,0,0,.224.813l4.9,2.75,0.031,0.018,4.9,2.75a0.622,0.622,0,0,0,.837-0.218C11.552,13.829,10.787,12.029,10.241,11.273Z' />
                  </svg>
                </Button>
              </span>
            </header>

            <menu className={ cx(css.nav__item, css.menu) }>
              <div className={ css.menu__wrap }>

                <div className={ css.accordion } onClick={ () => this.handleToggleExpandItem('demand') }>
                  <MenuItem className={ css.menu__item }>
                    <span className={ cx(css.menu__gtour) }>
                      <i className={ cx(css.menu__icon, css.materialIcons) }>view_list</i>
                      <span className={ cx(css.menu__slogan, css.aside__hideitem) }>Dropdown</span>
                    </span>
                  </MenuItem>
                </div>

                <div className={ css.accordion__hidden }>
                  <CSSTransitionGroup className={ cx({ [css.accordion__wrap]: true, [css.accordion__wrap_stat]: true, [css.accordion__wrap_open]: expandedItems.includes('demand') }) } component='div' transitionName={ { enter: css.accordion__wrap_show, leave: css.accordion__wrap_hide } } transitionEnterTimeout={ 800 } transitionLeaveTimeout={ 800 }>
                    {expandedItems.includes('demand') &&
                      <div className={ css.accordion__item }>

                        <NavLink className={ cx(css.menu__link, css.accordion__link) } activeClassName={ css.menu__link_active } to='/articles'>
                          <MenuItem className={ cx(css.menu__item, css.accordion__hidden) }>
                            <span className={ cx(css.menu__gtour, css.accordion__gtour) }>
                              <svg className={ cx(css.menu__icon, css.accordion__icon) } xmlns='http://www.w3.org/2000/svg' width='20' height='19' viewBox='0 0 20 19'>
                                <path d='M305.43,208h-3.206a1.022,1.022,0,0,0-1.014,1.029L301,210l-8,2a1.12,1.12,0,0,0-1-1h-4a1.275,1.275,0,0,0-1,1.295v7.422A1.262,1.262,0,0,0,288,221h4a1.263,1.263,0,0,0,.983-1.144L294,220v6a0.985,0.985,0,0,0,1,1h3a0.985,0.985,0,0,0,1-1v-4h2v1a0.985,0.985,0,0,0,1,1h4a0.985,0.985,0,0,0,1-1s0.02-12.2,0-14C307.091,208.462,306.007,208.006,305.43,208ZM291,219h-2v-6h2C291.022,212.6,291,218.914,291,219Zm6,6h-1v-4h1v4Zm4-5c-0.814-.246-7.809-1.942-8-2v-4l8-2v8Zm2,2c-0.014-.428-0.075-11.292,0-12,0.225-.007,2,0,2,0v12h-2Z' transform='translate(-287 -208)' />
                              </svg>
                              <span className={ cx(css.menu__slogan, css.aside__hideitem) }>Articles</span>
                            </span>
                          </MenuItem>
                        </NavLink>

                        <NavLink className={ cx(css.menu__link, css.accordion__link) } activeClassName={ css.menu__link_active } to='/fe'>
                          <MenuItem className={ cx(css.menu__item, css.accordion__hidden) }>
                            <span className={ cx(css.menu__gtour, css.accordion__gtour) }>
                              <svg className={ cx(css.menu__icon, css.accordion__icon) } xmlns='http://www.w3.org/2000/svg' width='20' height='16' viewBox='0 0 20 16'>
                                <path d='M289,326h16a2,2,0,0,1,2,2v9a2,2,0,0,1-2,2H289a2,2,0,0,1-2-2v-9A2,2,0,0,1,289,326Zm0,11h16v-9H289v9Zm4,3h8c1.8,0,2,1.011,2,2H291C291,340.9,291.205,340,293,340Z' transform='translate(-287 -326)' />
                              </svg>
                              <span className={ cx(css.menu__slogan, css.aside__hideitem) }>Not Found!</span>
                            </span>
                          </MenuItem>
                        </NavLink>

                      </div>
                    }
                  </CSSTransitionGroup>
                </div>

              </div>
            </menu>

          </nav>
        </section>
      </aside>
    );
  }
}
