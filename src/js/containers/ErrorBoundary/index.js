import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Button from 'components/Button';

import css from 'style/pages/NotFound.scss';

const deadTucan = require('../../../assets/img/dead-tucan.png');

export default class ErrorBoundary extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  state = {
    hasError: false,
  };

  componentDidCatch() {
    this.setState({
      hasError: true,
    });
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className={ css.member }>
          <div className={ css.member__item }>
            <section className={ css.member__inner }>
              <div className={ css.error } >
                <div className={ css.error__message } >
                  <div className={ css.error__img }>
                    <img src={ deadTucan } alt='Something went wrong' />
                  </div>
                  <div className={ css.error__description } >
                    <h1 className={ css.error__main_title }>Oops! Something went wrong</h1>
                  </div>
                  <Link to='/'>
                    <Button primaryClass={ true }>Back home</Button>
                  </Link>
                </div>
              </div>
            </section>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}
