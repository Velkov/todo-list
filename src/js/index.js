import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import cx from 'classnames';

import Routes from 'routes';
import DevTools from 'utils/dev/redux-dev-tools';
import { hideLoader } from 'utils/loader';
import { setItemTools, getItemTools } from 'utils';
import { HOST } from 'config/main';

import store from './redux/create';

import css from '../style/main';

hideLoader();

// Run project only our domain
if (!(Object.values(HOST).includes(window.location.host) || __SERVER__ === 'local')) {
  ReactDOM.render = () => {};
}

// Render it to DOM
ReactDOM.render(
  <Provider store={ store }>
    { __DEV_TOOL__ ?
      <div className={ css.devtools }>
        <div className={ cx(css.devtools__item, css.devtools__item_routes) }>
          <Routes />
        </div>
        <div className={ cx(css.devtools__item, css.devtools__item_tools) } id='devtools'>
          <button className={ css.devtools__button } onClick={ setItemTools }>tools</button>
          <DevTools />
        </div>
      </div> :
      <Routes />
    }
  </Provider>,
  document.getElementById('root')
);

getItemTools();
