export function setItemTools() {
  const toolsElem = document.querySelector('#devtools');
  const toolsElemToggle = toolsElem.classList.toggle('devtools__item_open');

  localStorage.setItem('panel-devtools-open', toolsElemToggle);
}

export function getItemTools() {
  if (localStorage.getItem('panel-devtools-open') === 'true') {
    document.querySelector('#devtools').classList.add('devtools__item_open');
  }
}

export function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
