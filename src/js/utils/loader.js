export function showLoader() {
  document.body.classList.remove('react-loading-completed');
}

export function hideLoader() {
  document.body.classList.add('react-loading-completed');
}
