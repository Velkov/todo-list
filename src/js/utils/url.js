import { forOwn } from 'lodash';
import qs from 'qs';

function normalizeFilters(filters) {
  const processed = {};

  forOwn(filters, (value, key) => {
    let valueProcessed;

    if (Array.isArray(value)) {
      valueProcessed = value || null;
    }

    processed[key] = valueProcessed || value;
  });

  return processed;
}

export function getQueryString({ limit, filters, pagination: { pageindex: page } }) {
  const filterProcessed = normalizeFilters(filters);

  const queryString = qs.stringify({
    filters: filterProcessed,
    limit,
    page,
  }, {
    skipNulls: true,
    addQueryPrefix: true,
    encode: false,
    arrayFormat: 'brackets',
  });

  const urlString = qs.stringify({
    filters: filterProcessed,
  }, {
    addQueryPrefix: true,
    encode: true,
    arrayFormat: 'brackets',
  });

  window.history.replaceState(null, '', urlString);

  return queryString;
}

export function getParamFromUrl(param, url = window.location.search) {
  const searchParams = qs.parse(url, { ignoreQueryPrefix: true });
  return param ? searchParams[param] : searchParams;
}
