const LOGIN = 'video/account/LOGIN';
const LOGIN_SUCCESS = 'video/account/LOGIN_SUCCESS';
export const LOGIN_ERROR = 'video/account/LOGIN_ERROR';

const LOGOUT = 'video/account/LOGOUT';
const LOGOUT_SUCCESS = 'video/account/LOGOUT_SUCCESS';
const LOGOUT_ERROR = 'video/account/LOGOUT_ERROR';

const LOAD_PROFILE = 'panel/account/LOAD_PROFILE';
const LOAD_PROFILE_SUCCESS = 'panel/account/LOAD_PROFILE_SUCCESS';
const LOAD_PROFILE_ERROR = 'panel/account/LOAD_PROFILE_ERROR';

const EXPIRED_SESSION = 'panel/account/EXPIRED_SESSION';

const initialState = {
  expired: false,
  auth: {
    login: {
      loading: false,
      loaded: false,
      error: {},
      data: {},
    },
    logout: {
      loading: false,
      loaded: false,
      error: {},
      data: {},
    },
  },
  profile: {
    loading: false,
    loaded: false,
    finished: false,
    isError: false,
    error: {},
    data: {},
  },
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        auth: {
          login: {
            ...state.auth.login,
            loading: true,
            loaded: false,
          },
        },
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        auth: {
          login: {
            ...state.auth.login,
            loading: false,
            loaded: true,
            fetched: true,
            data: action.result,
          },
        },
        profile: {
          ...state.profile,
          data: action.result,
        },
      };
    case LOGIN_ERROR:
      return {
        ...state,
        auth: {
          login: {
            ...state.auth.login,
            loading: false,
            loaded: false,
            fetched: true,
            error: {},
          },
        },
      };
    case LOGOUT:
      return {
        ...state,
        auth: {
          logout: {
            ...state.auth.logout,
            loading: true,
            loaded: false,
          },
        },
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        auth: {
          login: {
            ...state.auth.logout,
            loading: false,
            loaded: true,
            data: {},
          },
        },
      };
    case LOGOUT_ERROR:
      return {
        ...state,
        auth: {
          login: {
            ...state.auth.logout,
            loading: false,
            loaded: false,
            error: {},
          },
        },
      };
    case LOAD_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: true,
          finished: false,
          isError: false,
          error: {},
        },
      };
    case LOAD_PROFILE_SUCCESS:
      return {
        ...state,
        expired: false,
        profile: {
          ...state.profile,
          loading: false,
          loaded: true,
          finished: true,
          data: action.result,
        },
      };
    case LOAD_PROFILE_ERROR:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
          finished: true,
          isError: true,
          error: action.error,
        },
      };
    case EXPIRED_SESSION:
      return {
        ...state,
        expired: true,
      };
    default:
      return state;
  }
}

export function login(data) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_ERROR],
    url: '/api/login',
    method: 'POST',
    body: data,
  };
}

export function getAccount() {
  return {
    types: [LOAD_PROFILE, LOAD_PROFILE_SUCCESS, LOAD_PROFILE_ERROR],
    url: '/api/user',
  };
}

export function logout() {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_ERROR],
    url: '/api/logout',
  };
}

export function expiredSession() {
  return {
    type: EXPIRED_SESSION,
  };
}
