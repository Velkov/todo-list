const ADD = 'video/notifications/ADD';
const REMOVE = 'video/notifications/REMOVE';

const initialState = {
  data: [],
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD:
      return {
        ...state,
        data: [action.data, ...state.data],
      };
    case REMOVE:
      return {
        ...state,
        data: state.data.filter((item) => item.id !== action.data),
      };
    default:
      return state;
  }
}

export function add(data) {
  return {
    type: ADD,
    data,
  };
}

export function remove(data) {
  return {
    type: REMOVE,
    data,
  };
}
