import { createSelector, createSelectorCreator } from 'reselect';

import prevStateMiddleware from './middleware/prevState';

const loginFetching = state => state.account.auth.login.loading;
const loginLoaded = state => state.account.auth.login.loaded;

const profileFetching = state => state.account.profile.loading;
const profileFinished = state => state.account.profile.finished;
const profileLoaded = state => state.account.profile.loaded;
const profileError = state => state.account.profile.isError;

const expired = state => state.account.expired;

const createPrevSelector = createSelectorCreator((next) => prevStateMiddleware(next, (result, prevResult) => !result && prevResult));
const createProfileSelector = createSelectorCreator((func) => prevStateMiddleware(func, (result, prevResult) => result && !prevResult));

export const loginFinishLoadingSelector = createPrevSelector([loginFetching], value => value);
export const loginLoadedSelector = createSelector([loginLoaded], value => value);
export const loginFinishedSelector = createSelector([loginFinishLoadingSelector, loginLoaded], (valueA, valueB) => valueA && valueB);

export const userStartLoadingSelector = createProfileSelector([profileFetching], value => value);
export const userLoadingSelector = createSelector([profileFetching], value => value);
export const userFinishedSelector = createSelector([profileFinished], value => value);
export const userLoadedSelector = createSelector([profileLoaded], value => value);
export const userErrorSelector = createSelector([profileError], value => value);

export const expiredSelector = createSelector([expired], value => value);
